import {Context} from "koa";
import {getManager} from "typeorm";
import {Project} from "../entity/Project";

export default async function GetProjectInfoAction(ctx: Context) {
    const projectRepository = getManager().getRepository(Project);

    // load all posts
    const project = await projectRepository.findOne(ctx.params.id)
    if (!project){
        ctx.response.status =404
    }
    // return loaded posts
    ctx.body = project;
}