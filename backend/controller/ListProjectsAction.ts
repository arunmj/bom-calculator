import {Context} from "koa";
import {getManager} from "typeorm";
import {Project} from "../entity/Project";

export default async function ListProjectsAction(ctx: Context) {

    // get a post repository to perform operations with post
    const postRepository = getManager().getRepository(Project);

    // load all posts
    const projects = await postRepository.find();

    // return loaded posts
    ctx.body = projects;
}