import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";

@Entity()
export class Version {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    appVersion: string;

    @Column()
    projectId: string;

    @Column()
    createdUser: string;

    @Column()
    createdAt: Date;
    
    @Column()
    modifiedUser: string;
    
    @Column()
    modifiedAt: Date;

    @Column()
    input: String;
}
