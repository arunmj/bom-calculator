
import GetProjectInfoAction from "./controller/GetProjectInfoAction";
import ListProjectsAction from "./controller/ListProjectsAction";
import GoogleAuthCallbackAction from "./auth/GoogleAuthCallbackAction";
/**
 * All application routes.
 */
export const AppRoutes  = [
    {
        path: "/project/:id",
        method: "get",
        action: GetProjectInfoAction
    },
    {
        path: "/project",
        method: "get",
        action: ListProjectsAction
    },
    {
        path: "/auth/google-auth2-callback",
        method: "get",
        action: GoogleAuthCallbackAction
    },
];