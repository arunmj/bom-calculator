import * as angular from 'angular'
import './style.css'
import { Context } from '../bomcore/context';

var app = angular.module('bomc', []).config(function () {
});

app.controller('main', function ($scope, $http, $timeout) {
    $http.defaults.headers.post["Content-Type"] = "application/json";
    let $ctx : Context = (window as any).$bomcore.init()
    $scope.inputGroups = $ctx.GetGroupedInputSchema()
    console.log($scope.inputGroups);
    
    $scope.$watch('inputGroups', (newVal, oldVal) => {
            console.log(($ctx as any)._inputs);
            $ctx.Calculate()
		}, true);
})


// app.directive("user-input",)