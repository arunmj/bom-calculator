

export class ValidationError{
    message :string
    constructor(message:string){
        this.message = message
    }
}

export class Transform{
    value :string
    constructor(value:any){
        this.value = value
    }
}