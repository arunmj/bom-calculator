import { InputRegProps, RegisteredInputs } from "./Input";
import { deepCopy,  getClassMethodNamesOf, isFunction } from "../utils";
import { RegisteredFeatures, FeatureKlass } from "./Feature";
import { RegisteredServices, ServiceRegProps, ServiceKlass } from "./Service";
import { ValidationError, Transform } from "./throwable";

const calculateMethodPrefix = 'calculate'

export class Context{

    private _inputs : Record<string, InputRegProps>
    private _services : Record<string, ServiceKlass>
    private _features : Record<string, FeatureKlass>
    
    private _bom:any
    private _inputValues: Record<string, any>
    
    public get BOM() : any {
        return this._bom
    }
    
    public get input() {
        return this._inputValues
    }
    

    constructor(){
        this._loadInputs()
        this._loadFeatures()
        this._loadServices()
        console.log(this);
        
    }

    private _loadInputs(){
        this._inputs ={}
        RegisteredInputs.forEach(i => {
            const inp_copy:InputRegProps = deepCopy(i)
            inp_copy.modelFn = (value)=>{
                const isSetter = value != undefined
                if (isSetter){
                    console.debug(`valueEddited: ${inp_copy.id}`, value);
                    for(const f in inp_copy.validators){
                        const fn = inp_copy.validators[f]
                        if(!fn) continue
                        const v = fn(value)
                        if (v instanceof ValidationError){
                            // TODO: propograte to uI throu context
                            console.error(v)
                        }
                        if (v instanceof Transform){
                            value =  v.value
                        }
                    }
                    inp_copy._model = value
                }
                return inp_copy._model
            }
            inp_copy._model = inp_copy._default
            this._inputs[inp_copy.id] = inp_copy
        })
    }
    private _loadServices(){
        this._services = {}
        RegisteredServices.forEach(s => {
            const service: ServiceKlass= new s.klass
            Object.defineProperty(service,
                "ctx",
                { value: this, writable: false })
            console.debug(`loading service ${service.$.id}`)
            this._services[service.$.id] = service
        })
    }
    private _loadFeatures(){
        this._features = {}
        RegisteredFeatures.forEach(f => {
            const feature: FeatureKlass= new f.klass
            Object.defineProperty(feature,
                "ctx",
                { value: this, writable: false })
            console.debug(`loading feature ${feature.$.id}`)
            this._features[feature.$.id] = feature
        })
    }

    GetGroupedInputSchema() {
        let groups : {name:string, inputs: InputRegProps[]}[] = []
        const grouped: Record<string, InputRegProps[]> = {};
        for(let id in this._inputs ){
            const inp = this._inputs[id]
            if (!grouped[inp.group])
                grouped[inp.group] = []
            grouped[inp.group].push(inp)
        }
       
        for(let name in grouped){
            groups.push({name, inputs:grouped[name] })
        }
        return groups
    }

    updateInputIntoContext(){
        this._inputValues =  {}
        for(const iid in this._inputs){
            const inp = this._inputs[iid]
            this._inputValues[inp.id] = inp._modelTranslatorFn(inp)
        }
    }

    Calculate(){
        this.updateInputIntoContext()       
        this._bom = {services:{}}
        for(let sid in this._services){
            const serv = this._services[sid]
            this._bom["services"][serv.$.id] = this._calulateBomFor(serv)
        }
        // TODO: features
        console.debug("calculated", this._bom,'input:', this._inputValues);
    }
    

    private _calulateBomFor(component: (ServiceKlass|FeatureKlass)){
        if(component.isEnabled && (!component.isEnabled())){
            console.warn("disabled", component);
            return null
        }
        const componentBoM = {memory:null, hdd:null, cpu:null}
        if(isFunction(component.memory)){
            const mem = component.memory()
            componentBoM.memory = mem
        }
        getClassMethodNamesOf(component).forEach(
            m=>{
                if(m.startsWith('calculate') && m.length > calculateMethodPrefix.length ) {
                    const propName = m.substr(calculateMethodPrefix.length).toLowerCase()
                    if(['cpu', 'memory', 'hdd'].includes(propName)){
                        // TODO move this to service decoration
                    }
                    const value = component[m]()
                    componentBoM[propName] = value
                }

            }
        )
        return componentBoM
    }
}
