import { isValue, isList, deepCopy } from "../utils";
import { StringLengthValidator, NumericalRangeValidator } from "./validation";

type InputType = 'number' | 'string' | 'list' | 'option' | 'bool'
interface InputProps {
    id: string,
    description: string,
    itemDescription?: string,
    type: InputType,
    required?: boolean,
    default?: any,
    min?: number,
    max?: number,
    group: string,
    item?: string|number,
    items?: (string|number)[],
    validator?: Function | null,
}

type ValueTranslatorFunc = (i: InputRegProps) => any
export interface InputRegProps {
    id: string
    type: InputType
    description: string,
    group: string,
    items?:  (string|number)[]
    modelFn?: any,
    hasChanged?:any,

    _default?: any,
    _model?: any,
    _modelTranslatorFn?: any,
    target: any,
    model?: any,
    validators: Record<any, Function>
    trasformer?: Function
}

export const RegisteredInputs: Map<string, InputRegProps> = new Map

export function Input(props: InputProps) {
    return (target, key, descriptor) => {
        let validator = props.validator
        let trasformer
        if (typeof (descriptor.value) == "function") {
            trasformer = descriptor.value
        }

        const canBeMultiple = ['list', 'option'].includes(props.type)

        let reg: InputRegProps
        if (RegisteredInputs.has(props.id)) {
            // same id already exists. look for list
            reg = RegisteredInputs.get(props.id)
            if (reg.type != props.type || !canBeMultiple)
                throw `cannot redefine input ${props.id}. same id already registered with ${reg}`
        }
        else {
            reg = {
                id: props.id, type: props.type, group: props.group, description: props.description,
                trasformer, target, validators: {}
            }
            RegisteredInputs.set(reg.id, reg)
        }

        const passedValidator = props.validator || undefined

        // TODO cleanup other things also

        // default value translation is is 1-to-1 mapping from model
        let modelTranslatorFn: ValueTranslatorFunc = (i) => i._model


        switch (props.type) {
            case 'list':
                const newListItems = processAdditionOfItems(props, reg)
                if (!reg._default)
                    reg._default = []
                props.default && newListItems.forEach(i => reg._default.push(i))
                modelTranslatorFn = (i) => {
                    const selected = {}
                    i._model && i._model.forEach(e => {
                        selected[e] = true
                    });
                    Object.defineProperty(selected, 'selected', { value: (s) => !!selected[s] })
                    return selected
                }
                break;
                
            case 'option':
                const newOptItems = processAdditionOfItems(props, reg)
                reg._default = props.default
                
            case 'string':
                reg.validators[0] = passedValidator
                reg.validators['$strlen'] = StringLengthValidator(props.min, props.max)
                reg._default = props.default
                break

            case 'number':
                reg.validators[0] = passedValidator
                reg.validators['$numrange'] = NumericalRangeValidator(props.min, props.max)
                reg._default = +props.default
                break

            case 'bool':
                reg.validators[0] = passedValidator
                reg._default = !!props.default
                break;


            default:
                throw `Not implemeneted option: ${props.type}`;
        }

        reg._modelTranslatorFn = modelTranslatorFn
    }
}



function processAdditionOfItems(props, reg: InputRegProps) {
    // get items from props
    if (!props.item && !(props.items && props.items.length > 1)) {
        throw `${props.id} : for list/option item(s) field should be specified in decorator`
    }

    if (reg.group != props.group) // TODO more check
        // since this input registration also goes on same bucket.
        throw `some props of input ${props.id} collides with, ${reg}`

    let items = []
    isValue(props.item) && items.push(props.item)
    isList(props.items) && props.items.forEach(i => items.push(i))

    if (!reg.items)
        reg.items = []
    items.forEach(i => {
        reg.items.push(i)
        reg.validators[i] = props.validator
    })
    return items
}
