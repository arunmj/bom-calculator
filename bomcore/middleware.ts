
export type LifecycleHook = "pre-calculation" | "post-calculation"

interface MiddlewareProps {
    id:string,
   name:string,
   hook:LifecycleHook
}

interface MiddlewareRegProps extends MiddlewareProps {
    service: any
}

export const RegisteredMiddlewares: Map<string, MiddlewareRegProps> = new Map

export function Middleware(props: MiddlewareProps) {
    return (target) => {
        if (!props.id)
            props.id = target.name

        if (RegisteredMiddlewares.has(props.id))
            throw `multiple serivices with id ${props.id}`

        const service = new target
        const reg: MiddlewareRegProps = { ...props, service }
        Object.defineProperty(service,
            "$reg",
            { value: reg, writable: false })
        console.debug(`registering service ${reg}`)
        RegisteredMiddlewares.set(reg.id, reg)
    }
}