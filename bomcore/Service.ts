import { Context } from "./context";
import { relative } from "path";

export interface ServiceKlass {
    memory?: () => (number | Record<string, number>)
    hdd?: () => (number | Record<string, number>)
    ram?: () => (number | Record<string, number>)
    isEnabled?: ()=>boolean
    $: ServiceRegProps,
    ctx: Context
}

interface ServiceProps {
    id?: string
    realm?: string,
    nodes?: string,
    requiredInputs?: string[]
}

export interface ServiceRegProps extends ServiceProps {
    id:string,
    realm:string,
    klass: any
}

export const RegisteredServices: Map<string, ServiceRegProps> = new Map

export function Service(props: ServiceProps) {
    return (klass) => {
        const id :string = props.id || klass.name
        const realm :string = props.realm || 'default'

        if (RegisteredServices.has(props.id))
            throw `multiple services with id ${props.id}`

        const reg: ServiceRegProps = { ...props, id, realm, klass }
        Object.defineProperty(klass.prototype,
            "$",
            { value: reg, writable: false })
        // console.debug(`registering service`, reg)
        RegisteredServices.set(reg.id, reg)
    }
}