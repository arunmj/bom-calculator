import { Context } from "./context";

export interface FeatureKlass{
    memory?: () => (number | Record<string, number>)
    hdd?: () => (number | Record<string, number>)
    ram?: () => (number | Record<string, number>)
    isEnabled?: ()=>boolean
    $ : FeatureRegProps,
    ctx: Context
}
interface FeatureProps {
    id?: string
    realm?: string,
    nodes?: string,
    requiredInputs?: string[]
}

export interface FeatureRegProps extends FeatureProps {
    klass: any
}

export const RegisteredFeatures: Map<string, FeatureRegProps> = new Map

export function Feature(props) {
    return (klass) => {
        if (!props.id)
            props.id = klass.name

        if (RegisteredFeatures.has(props.id))
            throw `multiple features with id ${props.id}`

        const reg: FeatureRegProps = { ...props, klass }
        Object.defineProperty(klass.prototype,
            "$",
            { value: reg, writable: false })
        // console.debug(`registering feature`, reg )
        RegisteredFeatures.set(reg.id, reg)
    }
}
