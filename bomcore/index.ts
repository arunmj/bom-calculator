import { Input, RegisteredInputs, InputRegProps } from "./Input";
import { Service, RegisteredServices } from "./Service";
import { Feature, RegisteredFeatures } from "./Feature";
import { strict } from "assert";
import { stringify } from "querystring";
import { Context } from "./context";
import {Transform,ValidationError} from "./throwable";


// In the browser 'this' is a reference to the global object, called 'window'.
// In Node 'this' is a reference to the module.exports object.

const isBrowser = window && window.alert

let ctx: Context

const $core = {
    RegisteredInputs,
    RegisteredServices,
    RegisteredFeatures,
    ctx,
    init  : ()=>{
        $core.ctx = new Context()
        return $core.ctx
    }
}


if (isBrowser){
    if((window as any).$bomcore)
        throw "FATAL: bomcore imported multiple times"

    window['$bomcore'] =  $core
}


export {Input, Service, Feature, $core, Transform, ValidationError }