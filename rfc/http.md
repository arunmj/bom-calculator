About HTTP endpoints
---

### static
* **/index.html**  
  as usual, bootstrap from here

* **/***  
    other static assets

### api

* **/api/v1/***  
    APis as follows

* **/api/v1/projects**  
    list out projects
    *ref. project entity*

* **/api/v1/projects/\<projectId\>**  
    list out projects
    *ref. project entity*