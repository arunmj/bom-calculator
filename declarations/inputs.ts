import { Input } from "../bomcore";

class Inputs {
    @Input({
        id: "noBC",
        description: "No. of Targeted BCs per day",
        type: 'number',
        default: 30,
        required: true,
        min: 10,
        max: 99999,
        group: "Big Data",
        validator: null
    })
    noBcInput(value,ctx) {
        return 5
     }

    @Input({
        id: "noRealTimeBC",
        description: "No. of Targeted BCs per day",
        required: true,
        type: 'number',
        default: 30,
        min: 10,
        max: 99999,
        group: "Big Data",
        validator: null
    })
    noRealTimeBCInput(value, ctx) {}

    @Input({
        id: "domainName",
        description: "Domain name",
        type: 'string',
        default: '',
        min: 3,
        max: 30,
        group: "Installation"
    })
    domainnameInput(value, ctx) {}

    @Input({
        id: "isIMEnabled",
        description: "IM Enabled",
        type: 'bool',
        default: true,
        required: true,
        group: "Big Data",
    })

    isIMEnabledInput(value,ctx) {}

    @Input({
        id: "envType",
        description: "Environment Type",
        type: 'bool',
        default: 'Physical',
        items: ['Physical', 'VirtualMachine','Cloud'],
        group: "Installation",
    })
    envType(value,ctx) {}

    @Input({
        id: 'cpuPerServer',
        type: 'option',
        default: 'Default',
        items: ["Default",8, 12, 16, 20, 24, 28, 32, 36, 40, 44, 48, 52, 56, 64, 80],
        description: "CPU per server",
        group: 'Installation'
    })
    cpuPerServerInput(){}

    @Input({
        id: 'ramPerServer',
        type: 'option',
        default: 'Default',
        items:  ["Default",48, 64, 96, 128, 160, 192, 224, 256, 320, 384, 480, 512],
        description: "RAM per server",
        group: 'Installation'
    })
    ramPerServerInput(){}
}
