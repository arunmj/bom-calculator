import { toGB } from "../../utils";
import { Service, Input } from "../../bomcore";


@Service({
    realm: 'mesos',
})
class ConfigServer{
    ctx: any

    isRequired() {
        return true
    }

    cpu() {
        return this.memory() * 0.5
    }

    memory() {
        return this.hdd() * 0.75
    }

    hdd() {
        return 0
    }
}