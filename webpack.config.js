const path = require('path');
const fs = require('fs')
const CopyWebpackPlugin = require('copy-webpack-plugin')

// console.debug(process.versions)

const declarationDir = process.env.BOM_DECLARTION_DIR || "./declarations"

let declarations = []
function getDeclarations(dir){
    fs.readdirSync(dir).forEach(fname => {
        if(fname.startsWith("."))
            return
        const relpath = "." + path.sep + path.relative(__dirname, path.resolve(dir, fname))
        if (fname.match(/.*\.ts$/)){
            console.log("found: ", relpath);
            declarations.push(relpath)
            return
        }
        if(fs.statSync(relpath).isDirectory()){
            getDeclarations(relpath)
        }
    });
}

getDeclarations(declarationDir)

module.exports = {
    mode: "development",
    // mode: "production",
    entry: {
        bomc: declarations,
        // backend : './backend/index.ts',
        frontend: ['./frontend/app.ts', './frontend/index.html' ],

    },
    devtool: 'inline-source-map',
    module: {
        rules: [{
                test: /\.ts$/,
                use: 'ts-loader',
                exclude: /node_modules/
            },
            {
                test: /\.html$/,
                loader: "html-loader"
            },
            {
                test: /\.css$/,
                loader: "css-loader"
            }
        ]
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js']
    },
    output: {
        filename: '[name].bundle.js',
        path: path.resolve(__dirname, 'dist')
    },

    plugins: [
        new CopyWebpackPlugin([
            { from: './frontend/public' },
            { from: './frontend/index.html' },
            { from: './frontend/style.css' },
        ])
    ],
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
        host: '0.0.0.0',
        port: 9000
      }
};